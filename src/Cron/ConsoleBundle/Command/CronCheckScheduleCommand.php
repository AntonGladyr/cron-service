<?php

namespace Cron\ConsoleBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;

class CronCheckScheduleCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('cron:check-schedule')
            ->setDescription('...')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $options = array(

            CURLOPT_CUSTOMREQUEST  =>"GET",        //set request type post or get
            CURLOPT_POST           =>false,        //set to GET
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => false,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
        );

        $ch = curl_init('http://cnap-kremen.gov.ua/echerga/step2/239/0/1');
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        curl_close($ch);

        $crawler = new Crawler($content);
        $result = mb_convert_encoding(trim($crawler->filterXPath('//*[@id="step2"]')->text()), "UTF-8", "auto");
        if ($result !== "На жаль, вільний час відсутній!") {
            /** @var \Swift_Message $message */
            $message = \Swift_Message::newInstance()
                ->setSubject('ЦНАП')
                ->setFrom($this->getContainer()->getParameter('mailer_user'))
                ->setTo($this->getContainer()->getParameter('mailer_delivery_address'))
                ->setBody(
                    "Service is available! Yeaah! :)\r\nhttp://cnap-kremen.gov.ua/echerga/step2/23",
                    'text/plain');
            $this->getContainer()->get('mailer')->send($message);
        }

        $output->writeln('Done');
    }

}
